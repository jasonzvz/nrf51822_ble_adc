## About

This project will use nrf51822 nordic BLE4.0 chip to read ADC pin value and send them through BLE.

## How to build he project

1. Prepare the Linux environment
  - Arch linux is used as the environment setup example
  - Install the below package:
    * arm-none-eabi-gcc
    * arm-none-eabi-newlib
    * nrf5x-command-line-tools(AUR)
    * cp210x(AUR)
  - Modify the SDK toolchain configure in components/toolchain/gcc/Makefile.posix
    ```txt
    GNU_INSTALL_ROOT := /usr
    GNU_VERSION := 9.2.0
    GNU_PREFIX := arm-none-eabi
    ```
    Please configure it according to your host PC GNU settings

2. Compile and flash the firmware
  - Compile the project using the below commands:
    ```sh
    cd examples/ble_peripheral/ble_app_adc/pca10028/s110/armgcc
    make
    ```
  - Flash the hex binary into the nrf51822 chip using the below commands:
    ```sh
    cd examples/ble_peripheral/ble_app_adc/pca10028/s110/armgcc
    # flash the the softdevice, only need to flash once to make 
    # sure it is using s110_sdk8.0.0 version
    make flash_softdevice
    # flash the ble_app_adc application
    make flash
    ```
3. How to confirm the code is running as expected
  - Use UART to confirm
    * Use Baudrate: 115200
    * Console log should be printed
  - Use Mobile app to confirm
    * Download "nRF Toolbox" on your mobile device
    * go to "UART" and connect to "Nordic_ADC" device
    * go to "Show Log" and transmitted data can be seen



